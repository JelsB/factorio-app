module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    'plugin:vue/essential',
    '@vue/standard',
    '@vue/typescript/recommended'
  ],
  parserOptions: {
    ecmaVersion: 2020
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'vue/html-indent': 'warn',
    'vue/no-multi-spaces': 'warn',
    'vue/html-closing-bracket-spacing': 'warn',
    'vue/html-self-closing': 'warn',
    'vue/mustache-interpolation-spacing': 'warn',
    'vue/no-spaces-around-equal-signs-in-attribute': 'warn',
    'vue/v-bind-style': 'warn',
    'vue/v-on-style': 'warn',
    'vue/no-deprecated-scope-attribute': 'warn',
    'vue/no-deprecated-slot-attribute': 'warn',
    'vue/no-deprecated-slot-scope-attribute': 'warn',
    'vue/no-restricted-syntax': 'warn',
    'vue/no-static-inline-styles': 'warn',
    'vue/no-unsupported-features': 'warn',
    'vue/v-on-function-call': 'warn'
  },
  overrides: [
    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
        '**/tests/unit/**/*.spec.{j,t}s?(x)'
      ],
      env: {
        jest: true
      }
    }
  ]
}
