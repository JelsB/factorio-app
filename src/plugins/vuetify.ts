import Vue from 'vue'
import Vuetify from 'vuetify/lib'

Vue.use(Vuetify)

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#607d8b',
        secondary: '#3f51b5',
        accent: '#ffc107',
        error: '#cddc39',
        warning: '#009688',
        info: '#03a9f4',
        success: '#4caf50'
      }
    }
  }
})
